var version = "1.0.0";

$(document).ready( function () {
    //Write something to the console
    console.log("Praat Webinterface "+version);
    
    $(".PrintVersion").text(version);
    
});

var d = new Date,
day = d.getDate() < 10 ? "0" + d.getDate() : d.getDate(),
month = ((d.getMonth()+1) < 10 ? "0" + (d.getMonth()+1) : (d.getMonth()+1)),
year = d.getFullYear();