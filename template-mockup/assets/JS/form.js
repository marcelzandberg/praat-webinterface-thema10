$(document).ready( function () {
    
    if (typeof(Storage) !== "undefined") {
      // Code for localStorage/sessionStorage.
        
    } else {
        // Sorry! No Web Storage support..
        alert("No webstorage supported :(")
    }
    
    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.
    }
    else {
        alert('The File APIs are not fully supported in this browser.');
    }
    
    // Fill the randomhash input field with the randomhash
    if (localStorage.getItem("curr_randomhash") === null) {
        // Generate a randomhash
        var randomhashval = makeid();
        localStorage.setItem("curr_randomhash", randomhashval);
        $("#randomhash").val(randomhashval);
        $(".randomhashval").text(randomhashval);
    } else {
        $("#randomhash").val(localStorage.getItem("curr_randomhash"));
    }
    
    // autofill the current date
    $("#input-day").val(day);
    $("#input-month").val(month);
    $("#input-year").val(year);
    
    // When the user clicks on the date label or the date inputfield, set the focus to the day field
    $('#input-date, #FocusDay').click( function() {
        $(this).preventDefault;
        $("#input-day").focus();
    });
    
    // When the user has selected a file, set the filename as text on the button
    $('input[type="file"]').change( function() {
        var selector = "#filename-"+$(this).attr('id');
        $(selector).text($(this).val().split("\\")[2]);
    });
    
    // Event handler for a click on a switch (div with class "checkbox")
    $('.checkbox').click( function() {
        //toggle the class checked for this div
        $(this).toggleClass("checked");
        
        // construct a selector
        var selector = "#input-"+$(this).attr('id');
        
        // check if the checkbox is already checked or not and change the values
        if ($(selector).val() === 1) {
            $(selector).val("0");
        } else {
            $(selector).val("1");
        }
        
    });
    
    $('#submit-inputform, input[type="submit"]').click( function() {
        
        alert("Click to continue :)");
        
        // Add a loader here
        $(".grey-layer").fadeIn(500);
        $(".loader-container").show();
        
        var file1 = $("#file_one").val();
        var file2 = $("#file_two").val();
        var pat_nr = $("#input-patientnr").val();
        var date = $("#input-day").val()+$("#input-month").val()+$("#input-year").val();
        var split_channels = $("#input-split_channels").val();
        var cut_audio = $("#input-cut_audio").val();
        var normalize = $("#input-normalize").val();
        var type_script = $("#type_script").val();
        var randomhash = $("#randomhash").val();
        
        // you may validate your input
        if (file1.length === 0 ) {
            alert("Please choose a file1");
            $("#file_one").addClass("invalid-form");
            return false
        }
        
        if (file2.length === 0 ) {
            $("#file_two").addClass("invalid-form");
            alert("Please choose a file");
            return false
        }
        
        if (pat_nr.length === 0 ) {
            $("#pat_nr").addClass("invalid-form");
            alert("Please enter a patient nr");
            return false
        }
        
        /*if (date.length === 0 ) {
            date = day+"-"+month+"-"+year;
        }*/
        
        $("#input-boxcontent input").removeClass("invalid-form");
        
        $("#theform").submit();

        /*var formdata = new FormData();
        
        formdata.append("file1", file1);
        formdata.append("file2", file2);
        
        formdata.append("pat_nr", pat_nr);
        formdata.append("date", date);
        formdata.append("split_channels", split_channels);
        formdata.append("cut_audio", cut_audio);
        formdata.append("normalize", normalize);
        formdata.append("type_script", type_script);
        formdata.append("randomhash", randomhash);

        $.ajax({
            url: 'input.do', data: formdata,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                alert(data);
            },
            error: function(er){
                
            }
        });*/

        
        /*$.post("input.do",
        {
            file1: file1,
            file2: file2,
            pat_nr: pat_nr,
            date: date,
            split_channels: split_channels,
            cut_audio: cut_audio,
            normalize: normalize,
            type_script: type_script,
            randomhash: randomhash
        },function(data, status){
            if( data.indexOf( "Gelukt!" ) != -1 ) {
                // Do some shit
                
                // hide loader
            } else {
                alert(data);
            }
        });*/
        
        
    });
    
    $('.goToMaxLen').keyup(function (e) {
        // get # of chars in the input field
        var length = $(this).val().length;
        // get maxlength value
        var maxlen = $(this).attr('maxlength');
        
        // when backspace is pressed check if the cursor needs to move to the previeous input field
        if (e.keyCode === 8) {
            if (length == 0 && $(this).attr("class").indexOf( "dontgoback" ) == -1) {
                $(this).prev().prev().focus();
            }
        } else {
            if (length == maxlen) {
                $(this).next().next().focus();
            }
        }
    });

    
    
    
    
    
});



function makeid() {
    // initalize a new variable that will contain the randomhash string
    var text = "";
    // Store all the allowed characters in a string
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!?$%^&*#@";

    // Repeat 15 times, add a random character to the string
    for (var i = 0; i < 15; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}