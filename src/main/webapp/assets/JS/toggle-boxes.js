$(document).ready( function () {

    // Get the pathname and if it contains #output, show the output tab.
    var pathname = window.location.pathname;
    if (pathname.indexOf("#output") >= 0) {
        $("#output-boxcontent").show();
    }

    $(".tab").click( function () {
        // Generate a new selector of box content to show
        var boxcontentselector = "#"+$.trim($(this).text()).toLowerCase()+"-boxcontent";
        
        // Hide all box contents and show the new one
        $(".boxcontent").hide();
        $(boxcontentselector).show();
        
        // Set the tab that was clicked as active and all others to normal
        $(".tab").removeClass("active-tab");
        $(this).addClass("active-tab");
    });
    
});