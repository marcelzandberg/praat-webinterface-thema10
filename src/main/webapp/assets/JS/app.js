// Set the version number
var version = "1.0.0";

$(document).ready( function () {
    //Write something to the console
    console.log("Praat Webinterface "+version);

    // Display the version in the about popup, or where else needed.
    $(".PrintVersion").text(version);

    // Add a "this link is external" icon after each external link by checking if the hosts are the same
    // IT won't be added to images, to keep the design clean
    $( "a" ).each(function() {
        if (this.host !== window.location.host) {
            if($(this).find("img").length){
                return
            }
            $(this).after("<span class=\"no-underline\"> <i class=\"fas fa-external-link-alt no-underline\"></i></span>");
        }
    });

    // Check if the web app is ran as standsole (progressive web app) on an iOS device
    if (("standalone" in window.navigator) && window.navigator.standalone) {
        // The web app is in PWA mode. Now we will add some classes to fill up the statusbar and add some extra padding to the content
        $(".header").addClass("header-pwa");
        $("body").addClass("body-pwa");

        // Event handler for when the device is being rotated. The statusbar is hidden in landscape mode, so the extra padding isn't needed anymore.
        $( window ).on( "orientationchange", function( event ) {

            // Because of the transition animations in iOS, the viewport width and height change after a few seconds.
            // Therefore is a timeout of 50ms set before the viewport sizes are requested.
            setTimeout( function () {
                // Check if in landscape mode. If so, add the extra padding classes, else remove these classes
                if (window.innerWidth < window.innerHeight) {
                    $(".header > nav").removeClass("nav-pwa");
                    $(".header > nav").addClass("nav-pwa");
                    $(".header").removeClass("header-pwa");
                    $(".header").addClass("header-pwa");
                } else {
                    $(".header > nav").removeClass("nav-pwa");
                    $(".header").removeClass("header-pwa");
                }
            }, 50);

        });

    }

    
});

// Construct and format the current date. Set as global variable so it can be used in other JS files
var d = new Date,
day = d.getDate() < 10 ? "0" + d.getDate() : d.getDate(),
month = ((d.getMonth()+1) < 10 ? "0" + (d.getMonth()+1) : (d.getMonth()+1)),
year = d.getFullYear();