$(document).ready( function () {

    // These event handlers are only used when the webapp is used on a mobile device

    // This is a event handler for the mobile menu. If clicked on the ••• in the header it should show the menu.
    $("#toggleMenu").click( function () {
        $(".header nav").toggle();
    });

    //Hide the menu on mobile when the user touch the content of a page (outside the menu)
    $(".container").click( function() {
        $(".header nav").hide();
    });
    
})