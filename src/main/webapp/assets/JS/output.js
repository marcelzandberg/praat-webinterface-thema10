$(document).ready( function () {

    // If there is no last randomhash set, hide the notification and the output data;
    // Else it should show the notification, fill in the search field and get the output.
    if (localStorage.getItem("last_randomhash") === null) {

        $("#outputdiv").hide();
        $(".notification").hide();

    } else {
        var jobid = localStorage.getItem("last_randomhash");
        $("#search-job-output").val(jobid);
        $(".notification").show();
        getOutput(jobid);
    }

    // Keyup event handler for the JobID search form.
    $("#search-job-output").keyup( function () {
        // Get the value of the search field.
        var jobid = $(this).val();

        // Check if there are 15 characters entered. If so, get the output and remove the red border;
        // Else show a red border around the field and do nothing.
        if (jobid.length === 15) {
            getOutput(jobid);
            $(this).removeClass("invalid-form");
        } else {
            $(this).addClass("invalid-form");
        }
    });


});

// Function to get the output and details
function getOutput(jobid) {
    $("#requestoutputdiv").hide();
    $("#outputdiv").show();
    $("#jobid").text(jobid);

    GetDetails(jobid);
}

function GetDetails(jobid) {

    // Send AJAX post request to the output servlet. Get the content of the JSON file
    $.post( "output.get", {
        jobID: jobid,
        file: "data.json"
    }).done(function( data ) {

        // If server returns our error text, hide the output elements and show a message, since there is nothing found.
        if (data === "Error: File not found") {
            $("#outputdiv").hide();
            $("#requestoutputdiv").show();
            $("#requestoutputdiv > h3").text("Nothing found for this Jobid");
            return
        }

        // Run the getfile functions
        GetSoundPNG(jobid);
        GetSpectogramPNG(jobid);
        GetSpectogram_0_03PNG(jobid);

        // Parse the JSON
        var details = JSON.parse(data);

        // Change booleans to human readable strings
        for (key in details[0]) {
            var tbselector = "#tb-"+key;
            var value = details[0][key];
            if (value == "1") {
                value = "Yes";
            } else if (value == "0") {
                value = "No";
            }
            $(tbselector).text(value);
        }

        // Generate the file URL's
        var downloadprefix = "./download.get?randomhash="+details[0].randomhash+"&fileName=";
        var csvFN = details[0].pat_nr+"_"+details[0]["date"].split("-")[2]+"."+details[0]["date"].split("-")[1]+"."+details[0]["date"].split("-")[0]+".csv";
        var wav_oneFN = details[0].wav_one+".wav";
        var wav_twoFN = details[0].wav_two+".wav";

        // Assign the download links to the buttons
        $("#download-csv").attr("href", downloadprefix + csvFN);
        $("#download-wav_one").attr("href", downloadprefix + wav_oneFN);
        $("#download-wav_two").attr("href", downloadprefix + wav_twoFN);
        $("#download-soundpdf").attr("href", downloadprefix + "sound.pdf");
        $("#download-soundpng").attr("href", downloadprefix + "sound.png");
        $("#download-spectrogrampng").attr("href", downloadprefix + "spectogram.png");
        $("#download-spectrogrampng_0-03").attr("href", downloadprefix + "spectrogram_0.03.png");

    }).fail(function( data ) {
        // If the server gives  an error, show an alert
        alert("Couldn't get details\n\n"+data);
    });
}

function GetSoundPNG(jobid) {
    //send post req for data
    $.post( "output.get", {
        jobID: jobid,
        file: "sound.png"
    }).done(function( data ) {
        // Set data as source for the image
        $("#soundimage").attr("src", "data:image/png;base64," + data);
    });
}

function GetSpectogramPNG(jobid) {
    //send post req for data
    $.post( "output.get", {
        jobID: jobid,
        file: "spectrogram.png"
    }).done(function( data ) {
        // Set data as source for the image
        $("#spectrogramimage").attr("src", "data:image/png;base64," + data);
    });
}

function GetSpectogram_0_03PNG(jobid) {
    //send post req for data
    $.post( "output.get", {
        jobID: jobid,
        file: "spectrogram_0.03.png"
    }).done(function( data ) {
        // Set data as source for the image
        $("#spectrogram_0-03image").attr("src", "data:image/png;base64," + data);
    });
}

// Resize the images to the viewport
$(window).resize(function () {
    var viewportWidth = $(window).width();
    if (viewportWidth < 751) {
        var nw_imgwidth = 0.75 * viewportWidth;
        var nw_imgheight = ((nw_imgwidth/3)*2);
        $("#soundimage").attr("width", nw_imgwidth).attr("height", nw_imgheight);
        $("#spectrogramimage").attr("width", nw_imgwidth).attr("height", nw_imgheight);
        $("#spectrogram_0-03image").attr("width", nw_imgwidth).attr("height", nw_imgheight);
    } else {
        $("#soundimage").attr("width", "640").attr("height", "480");
        $("#spectrogramimage").attr("width", "640").attr("height", "480");
        $("#spectrogram_0-03image").attr("width", "640").attr("height", "480");
    }
});
