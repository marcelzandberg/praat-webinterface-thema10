$(document).ready( function () {
    
    // Click event handler for every class that starts with openpopup-
    $("[class^=openpopup-]").click( function () {
        
        // Generate a selector of the popup that needs to be shown
        var popupselector = "#"+$.trim($(this).attr('class').split("openpopup-")[1]).toLowerCase()+"-popup";

        $(".popup").hide(); // hide all popups so they don't lay over each other
        $(".grey-layer").fadeIn(500).removeClass("hideonload"); // Fade the grey layer in
        $(popupselector).show().removeClass("hideonload"); // Fade the popup in

    });
    
    $(".close-popup, .grey-layer").click( function () {
        // Hide all popups and the grey layer
        $(".popup").hide();
        $(".grey-layer").hide();
        
    });
    
});