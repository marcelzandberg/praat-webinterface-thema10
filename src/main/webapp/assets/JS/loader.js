// When the submit button is clicked, show the grey overlay to prevent the user from making any changes and give feedback that the form is being submitted.
$("#submit-inputform").click(function() {
    $(".grey-layer").fadeIn(500); // Fade the grey layer in
});