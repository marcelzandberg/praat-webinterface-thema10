$(document).ready( function () {
    
    if (typeof(Storage) !== "undefined") {
      // Code for localStorage/sessionStorage.
        
    } else {
        // Sorry! No Web Storage support..
        alert("No webstorage supported :(\nThis app may not properly work on your device.")
    }
    
    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.
    }
    else {
        alert('The File APIs are not fully supported in this browser.');
    }
    
    // Fill the randomhash input field with the randomhash
    if (localStorage.getItem("curr_randomhash") === null) {
        // Generate a randomhash
        var randomhashval = makeid();
        localStorage.setItem("curr_randomhash", randomhashval);
        $("#randomhash").val(randomhashval);
        $(".randomhashval").text(randomhashval);
    } else {
        $("#randomhash").val(localStorage.getItem("curr_randomhash"));
        $(".randomhashval").text(localStorage.getItem("curr_randomhash"));
    }
    
    // autofill the current date
    $("#input-day").val(day);
    $("#input-month").val(month);
    $("#input-year").val(year);
    
    // When the user clicks on the date label or the date inputfield, set the focus to the day field
    $('#input-date, #FocusDay').click( function() {
        $(this).preventDefault;
        $("#input-day").focus();
    });
    
    // When the user has selected a file, set the filename as text on the button
    $('input[type="file"]').change( function() {
        var selector = "#filename-"+$(this).attr('id');
        // Get the filename
        var fn = $(this).val().split("\\")[2];
        $(selector).text(fn).addClass("small-font");

        // Test if the file name is allowed. Currently the backend only handles filenames with a specific pattern.
        var regex = new RegExp('(TASCAM_\\d+S\\d{2})(D06\\.wav)|(TASCAM_\\d+S\\d{2})(\\.wav)');

        // Test the regex. If it matches, show an alert and reset the field.
        if (!regex.test(fn)) {
            $(selector).text("Click here to select").removeClass("small-font");
            $(this).val('');
            alert('Wrong file name.\nSee the help section for details.');
        }

    });
    
    // Event handler for a click on a switch (div with class "checkbox")
    $('.checkbox').click( function() {

        // Check if the item may be clicked
        if ($(this).hasClass("not-allowed")) {
            return
        }

        //toggle the class checked for this div
        $(this).toggleClass("checked");
        
        // construct a selector
        var selector = "#input-"+$(this).attr('id');
        
        // check if the checkbox is already checked or not and change the values
        if ($(selector).val() == 1) {
            $(selector).val("0");
            $(".checkbox").removeClass("not-allowed").attr("title","");
        } else {
            $(selector).val("1");
            $(".checkbox").addClass("not-allowed").attr("title","To select this option, please disable the selected option first");
            $(".checked").removeClass("not-allowed").attr("title","");
        }
        
    });

    // Add visual form validation to the required input fields (see readme on how to apply this function to a inputfield).
    $('input[data-req="true"]').keyup( function () {
        // Get the minimum required length
        var minlen = $(this).attr("data-req_min")

        // Check if the current length is sufficient. If so, remove the invalid border class; if not, add the invalid border class
        if ($(this).val().length < minlen ) {
            $(this).addClass("invalid-form");
            return false
        } else {
            $(this).removeClass("invalid-form");
        }
    });

    // Event handler for click on the submit input form button
    $('#submit-inputform').click( function() {

        // Get the values.
        var file1 = $("#file_one").val();
        var file2 = $("#file_two").val();
        var pat_nr = $("#input-patientnr").val();
        var date = $("#input-day").val()+$("#input-month").val()+$("#input-year").val();
        var split_channels = $("#input-split_channels").val();
        var cut_audio = $("#input-cut_audio").val();
        var normalize = $("#input-normalize").val();
        var type_script = $("#type_script").val();
        var randomhash = $("#randomhash").val();

        // Concat the date fields in a new field to send it as one string.
        $("#input-date-full").val(date);

        // Validate the input before sending. Gives a alert and give the inputfields a red border if needed.
        // It also prevents submitting the form by returning false.
        if (file1.length === 0 ) {
            alert("Please choose a file1");
            $("#file_one").addClass("invalid-form");
            return false
        }
        
        if (file2.length === 0 ) {
            $("#file_two").addClass("invalid-form");
            alert("Please choose a file");
            return false
        }
        
        if (pat_nr.length === 0 ) {
            $("#pat_nr").addClass("invalid-form");
            alert("Please enter a patient nr");
            return false
        }
        
        if (date.length !== 8 ) {
            $(".date-input-container").addClass("invalid-form");
            alert("Please enter a valid date");
            return false
        }

        // Show the loader and a grey overlay
        $('.grey-layer-noclick').show().removeClass("hideonload");
        $('.loader-container').show().removeClass("hideonload");

        // Remove the red borders, if they were placed around invalid inputfields
        $("#input-boxcontent input").removeClass("invalid-form");

        // Set the current randomhash as last
        localStorage.setItem("last_randomhash", localStorage.getItem("curr_randomhash"));

        // Clear the current randomhash so there will be a new one created when the page is loaded again
        localStorage.removeItem("curr_randomhash");

        // Submit the form
        $("#theform").submit();

        
    });
    
    $('.goToMaxLen').keyup(function (e) {
        // get # of chars in the input field
        var length = $(this).val().length;
        // get maxlength value
        var maxlen = $(this).attr('maxlength');
        
        // when backspace is pressed check if the cursor needs to move to the previeous input field
        if (e.keyCode === 8) {
            if (length == 0 && $(this).attr("class").indexOf( "dontgoback" ) == -1) {
                $(this).prev().prev().focus();
            }
        } else {
            if (length == maxlen) {
                // if the max length is reached, set the focus on the next input
                $(this).next().next().focus();
            }
        }
    });
    
});



function makeid() {
    // initalize a new variable that will contain the randomhash string
    var text = "";
    // Store all the allowed characters in a string
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    // Repeat 15 times, add a random character to the string
    for (var i = 0; i < 15; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}