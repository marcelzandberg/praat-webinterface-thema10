<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- © 2019 Bas Kasemir and Marcel Zandberg. -->
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="viewport-fit=cover, width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Praat Webinterface</title>
    <link href="./assets/images/icons/Praat-icon.png" rel="icon" type="image/png">
    <link href="./assets/CSS/app.css" rel="stylesheet" type="text/css">
    <link href="./assets/CSS/header.css" rel="stylesheet" type="text/css">
    <link href="./assets/CSS/box.css" rel="stylesheet" type="text/css">
    <link href="./assets/CSS/popup.css" rel="stylesheet" type="text/css">
    <link href="./assets/CSS/form.css" rel="stylesheet" type="text/css">
    <link href="./assets/fontawesome-free-5.7.0-web/css/all.css" rel="stylesheet">
    <script src="./assets/JS/jquery-3.3.1.min.js"></script>
    <script src="./assets/JS/app.js"></script>
    <script src="./assets/JS/toggle-menu.js"></script>
    <script src="./assets/JS/toggle-boxes.js"></script>
    <script src="./assets/JS/popup.js"></script>
    <script src="./assets/JS/form.js"></script>
    <script src="./assets/JS/output.js"></script>

    <!-- Apple iOS devices web app configuration, the Android devices config is located in tje  -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Praat">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/icons/ios/Praat-icon-180px.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./assets/images/icons/ios/Praat-icon-120px.png">
    <link rel="apple-touch-icon" sizes="167x167" href="./assets/images/icons/ios/Praat-icon-167px.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./assets/images/icons/ios/Praat-icon-152px.png">
    <link rel="apple-touch-icon" sizes="1024x1024" href="./assets/images/icons/ios/Praat-icon-1024px.png">

    <!-- Splashscreens for the demo -->
    <!-- iPhone Xr (828px x 1792px) -->
    <link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" href="./assets/images/splash-iph-xr.png">
    <!-- iPhone 5/5S/SE -->
    <link rel="apple-touch-startup-image" href="./assets/images/splash-iph-se.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)">


</head>

<body>
<div class="PWA"></div>
<div class="header no_selection">
    <h3 class="cur-arrow">Praat Webinterface</h3>
    <nav class="no_selection">
        <h3 class="openpopup-about">About</h3>
        <h3 class="openpopup-help">Help</h3>
    </nav>
    <h3 id="toggleMenu">
        <i class="fas fa-ellipsis-h"></i>
    </h3> </div>
<div class="grey-layer no_selection hideonload"></div>
<div class="grey-layer-noclick no_selection hideonload"></div>
<div class="popup hideonload" id="about-popup">
    <div class="close-popup no_selection"> <i class="fas fa-times"></i> </div>
    <h3><i class="fas fa-info-circle"></i>&nbsp;About Praat Webinterface</h3>
    <p>Version: <span class="PrintVersion"></span></p>
    <p>This webinterface for the Praat program (<a href="http://praat.org" target="_blank" >visit site</a>) is developed by two bioinformatics students of the Hanze University of Applied Sciences Groningen, The Netherlands.</p>
    <p>For feedback and / or questions, don't hesitate to send us an email:
        <br>
        <b>Bas Kasemir</b>&nbsp;&nbsp;&nbsp;<a href="mailto:b.h.n.kasemir@st.hanze.nl" target="_blank" title="Mail Bas Kasemir"><i class="fa fa-envelope"></i></a>&nbsp;<a href="https://www.linkedin.com/in/bas-kasemir-b4277b99/" target="_blank" title="LinkedIn profiel of Bas Kasemir"><i class="fab fa-linkedin"></i></a>
        <br><b>Marcel Zandberg&nbsp;&nbsp;&nbsp;<a href="mailto:m.a.zandberg@st.hanze.nl" target="_blank" title="Mail Marcel Zandberg"><i class="fa fa-envelope"></i></a></b>
    </p>

    <a href="https://hanze.nl" target="_blank" title="Visit Hanze.nl">
        <img src="./assets/images/HG-logo-EN-Orange-Black.png" height="50" width="226" alt="Hanze" class="aff-logo">
    </a>

    <a href="https://umcutrecht.nl" target="_blank" title="Visit umcutrecht.nl">
        <img src="./assets/images/UMCU-logo.png" height="50" width="138" alt="UMCU" class="aff-logo">
    </a>

    <a href="https://umcg.nl" target="_blank" title="Visit umcg.nl">
        <img src="./assets/images/UMCG-logo.png" height="50" width="91" alt="UMCG" class="aff-logo">
    </a>

    <br><br><br>

    <p class="copy">Handcrafted with &hearts; in Groningen<br>&copy; Copyright 2019 Hanze University of Applied Sciences</p>
</div>
<div class="popup hideonload" id="help-popup">
    <div class="close-popup no_selection"> <i class="fas fa-times"></i> </div>
    <h3><i class="fas fa-question-circle"></i>&nbsp;Help</h3>
    <p><b>Input tab:</b></p>
    <p>Praat needs 2 audio files with TASCAM_nummerS12/S34_6decibel.wav as file name to perform, one normal and one 6db lower. file1 = "TASCAM_nummerS12.wav" and file2 = "TASCAM_nummerS34_d06.wav". The volume of this file should be 6 decibel lower than the file1. </p>
    <p>For the metadata file: a patient number is needed, this can be anything. also a date is needed, fill in the date of when the audio files are created</p>
    <p>In the advanced setting there are 3 options. You can only use one per run or you can choose to don't using them</p>
    <p>For now only the Praat script of Dhr Voppel, A.E from umcUtrecht is used.</p>
    <p></p>
    <p><b>Output tab:</b></p>
    <p>The output generates a table with the content you provided to the server. If the images that will be created arent there yet just give it a second or two ad refresh the page</p>
    <p>Everything can be downloaded with the download buttons at the bottom of the web application</p>
</div>

<div class="popup loader-container hideonload">
    <h3>Processing...</h3>
    <p>Your files are now being processed. This page will automatically refresh when the process is finished. You may close this page aswell and check your result later with the following code: <b class="randomhashval"></b></p>
</div>

<div class="container">
    <h1>Welcome</h1>
    <p class="intro">
        Praat is a computer program with which you can analyse, synthesize and manipulate speech, and create high-quality pictures for your articles and thesis.
        <br>
        Click on <i class="openpopup-help">help</i> in the menu (top right of the window) to read a get started manual.</p>
    <br>
    <div class="notification information hideonload">
        <h3><i class="fas fa-info-circle"></i>&nbsp;Output found</h3>
        <p>There is a previous job found. Click on the output tab to view.</p>
    </div>

    <div class="box">
        <div class="box-head">
            <div class="tabs no_selection">
                <div class="tab active-tab"> Input </div>
                <div class="tab"> Output </div>
            </div>
        </div>
        <div class="boxcontent" id="input-boxcontent">
            <h1>Input form</h1>
            <br>
            <h4>Audio files *</h4>

            <form action="input.do" method="post" enctype="multipart/form-data" id="theform">
                <div class="fakeinput file-input-container">

                    <label for="file_one" class="for-field field-label"><i class="fas fa-file-audio"></i>&nbsp;File1:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hideeonload-important" id="filename-file_one">Click here to select</span></label>

                    <input type="file" name="file1" id="file_one" data-req="true" data-req_min="1" accept="audio/wav" class="hideonload-important">

                    <i class="far fa-question-circle tooltip help-icon">
                        <span class="tooltiptext no_selection">Upload a *.wav file.</span>
                    </i>
                </div>

                <div class="fakeinput file-input-container">

                    <label for="file_two" class="for-field filelabel field-label"><i class="fas fa-file-audio"></i>&nbsp;File2:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hideeonload-important" id="filename-file_two">Click here to select</span></label>

                    <input type="file" name="file2" id="file_two" data-req="true" data-req_min="1" accept="audio/wav" class="hideonload-important">

                    <i class="far fa-question-circle tooltip help-icon">
                        <span class="tooltiptext no_selection">Upload a second *.wav file. The volume of this file should be 6 decibel lower than the file1.</span>
                    </i>
                </div>

                <br>
                <br>
                <h4>Metadata *</h4>
                <div class="fakeinput">
                    <i class="far fa-question-circle tooltip help-icon">
                        <span class="tooltiptext no_selection">Enter the patient identifier.</span>
                    </i>
                    <label for="input-patientnr" class="for-field field-label">
                        <i class="fa fa-user"></i>&nbsp;Pat. id
                    </label>
                    <input type="text" name="pat_nr" id="input-patientnr" data-req="true" data-req_min="1" placeholder="P002" autocomplete="off">
                </div>


                <div class="fakeinput date-input-container">

                    <label id="FocusDay" class="for-field field-label"><i class="fa fa-calendar-day"></i>&nbsp;Date</label>

                    <input id="input-date" type="tel" data-req="true" autocomplete="off" class="input-labelonly">

                    <label for="input-day" class="label-small">Day</label>
                    <input id="input-day" type="tel" data-req="true" data-req_min="2" placeholder="DD" autocomplete="off" class="input-small goToMaxLen dontgoback date-input" maxlength="2">

                    <label for="input-month" class="label-small">Month</label>
                    <input id="input-month" type="tel" data-req="true" data-req_min="2" placeholder="MM" autocomplete="off" maxlength="2" class="input-small goToMaxLen date-input">

                    <label for="input-year" class="label-small">Year</label>
                    <input id="input-year" type="tel" data-req="true" data-req_min="4" placeholder="YYYY" autocomplete="off" maxlength="4" class="input-double-small goToMaxLen date-input">

                    <i class="far fa-question-circle tooltip help-icon">
                        <span class="tooltiptext no_selection">The date of the recording in the format DD-MM-YYYY. Default is today's date and is auto filled.</span>
                    </i>

                </div>

                <br>
                <input name="date" id="input-date-full" type="text" autocomplete="off" hidden class="hideonload-important" value="00000000">
                <br>
                <h4>Advanced options</h4>

                <div class="checkbox-container">

                    <div class="no_selection checkbox" id="split_channels">

                        <div class="backbar"><span class="onlabel">&nbsp;On</span></div>

                        <div class="switch"></div>

                        <label>
                            Split channels&nbsp;<i class="far fa-question-circle tooltip"><span class="tooltiptext no_selection">Turn on if your *.wav file is a stereo recording</span></i>
                        </label>

                    </div>
                    <br><br>
                    <input type="checkbox" name="split_channels" value="0" id="input-split_channels" class="hideonload" checked>

                </div>




                <div class="checkbox-container">

                    <div class="no_selection checkbox" id="cut_audio">

                        <div class="backbar"><span>&nbsp;On</span></div>

                        <div class="switch"></div>

                        <label>
                            Cut audio at 15 min&nbsp;<i class="far fa-question-circle tooltip"><span class="tooltiptext no_selection">Turn on if you want to cut the audio file after 15 minutes.</span></i>
                        </label>

                    </div>
                    <br><br>
                    <input type="checkbox" name="cut_audio" value="0" id="input-cut_audio" class="hideonload" checked>

                </div>






                <div class="checkbox-container">

                    <div class="no_selection checkbox" id="normalize">

                        <div class="backbar"><span>&nbsp;On</span></div>

                        <div class="switch"></div>

                        <label>
                            Normalize the audio&nbsp;<i class="far fa-question-circle tooltip"><span class="tooltiptext no_selection">Turn on if you want to normailze the audio file.</span></i>
                        </label>

                    </div>
                    <br><br>
                    <input type="checkbox" name="normalize" value="0" id="input-normalize" class="hideonload" checked>

                </div>

                <div class="select-wrapper">
                    <i class="fas fa-caret-down"></i>
                    <label for="type_script" class="for-field"><i class="fas fa-cogs"></i>&nbsp;Script:</label>
                    <select id="type_script" name="type_script">

                        <option selected disabled>-- Installed scripts --</option>
                        <option selected value="s1">Praat Script Syllable Nuclei v2</option>
                    </select>

                    &nbsp;<i class="far fa-question-circle tooltip"><span class="tooltiptext no_selection">Choose a praat script.</span></i>
                </div>

                <input type="text" name="randomhash" id="randomhash" class="hideonload-important">



                <br><br><br><br>

                <div class="btn" id="submit-inputform">
                    <i class="fas fa-arrow-right"></i>Submit
                </div>

            </form>

        </div>
        <div class="boxcontent hideonload" id="output-boxcontent">
            <h1>Output data</h1>
            <br>
            <h4>Look up a previous job</h4>
            <label for="search-job-output" class="for-field"><i class="fas fa-fingerprint"></i>&nbsp;Job ID:</label>
            <input type="text" id="search-job-output" maxlength="15" autocomplete="off" placeholder="Enter a hashcode">
            <br><br>
            <div id="requestoutputdiv">
                <h4>There is no last Job ID found</h4>
            </div>
            <div id="outputdiv">
                <h4>Job ID: <span id="jobid"></span></h4>
                <p>Here you will find the outputs of the Praat software. Below the images are download links.</p>

                <br><br>
                <div class="overflow">
                    <table>
                        <tr>
                            <th>Key</th>
                            <th>Data</th>
                        </tr>
                        <tr>
                            <td>Jobid</td>
                            <td id="tb-randomhash"></td>
                        </tr>
                        <tr>
                            <td>Pat. id</td>
                            <td id="tb-pat_nr"></td>
                        </tr>
                        <tr>
                            <td>Date</td>
                            <td id="tb-date"></td>
                        </tr>
                        <tr>
                            <td>Split channels</td>
                            <td id="tb-split_channels"></td>
                        </tr>
                        <tr>
                            <td>Cut audio</td>
                            <td id="tb-cut_audio"></td>
                        </tr>
                        <tr>
                            <td>Normalize</td>
                            <td id="tb-normalize"></td>
                        </tr>
                        <tr>
                            <td>Script id</td>
                            <td id="tb-type_script"></td>
                        </tr>
                        <tr>
                            <td>Filename 1</td>
                            <td id="tb-wav_one"></td>
                        </tr>
                        <tr>
                            <td>Filename 2</td>
                            <td id="tb-wav_two"></td>
                        </tr>
                    </table>
                </div>



                <br><br>



                <h4>Sound:</h4>
                <img src="" height="480" width="640" alt="Sound may not be created yet or this jobid doesn't exist. Please check again in a few minutes." id="soundimage">
                <br><br>
                <h4>Spectrogram:</h4>
                <img src="" height="480" width="640" alt="spectrogram may not be created yet or this jobid doesn't exist. Please check again in a few minutes." id="spectrogramimage">

                <br><br>
                <h4>Spectrogram 0.03:</h4>
                <img src="" height="480" width="640" alt="spectrogram_0.03 may not be created yet or this jobid doesn't exist. Please check again in a few minutes." id="spectrogram_0-03image">

                <br><br><br>

                <div class="downloads-container">
                    <h3>Downloads</h3>
                    <br><br>
                    <a href="" title="Download" id="download-csv" download="">
                        <div class="btn download-btn">
                            <i class="fas fa-arrow-down"></i>Download CSV
                        </div>
                    </a>

                    <a href="" title="Download" id="download-wav_one" download="">
                        <div class="btn download-btn">
                            <i class="fas fa-arrow-down"></i>Download File1
                        </div>
                    </a>

                    <a href="" title="Download" id="download-wav_two" download="">
                        <div class="btn download-btn">
                            <i class="fas fa-arrow-down"></i>Download File2
                        </div>
                    </a>

                    <a href="" title="Download" id="download-soundpdf" download="">
                        <div class="btn download-btn">
                            <i class="fas fa-arrow-down"></i>Download sound PDF
                        </div>
                    </a>

                    <a href="" title="Download" id="download-soundpng" download="">
                        <div class="btn download-btn">
                            <i class="fas fa-arrow-down"></i>Download sound PNG
                        </div>
                    </a>

                    <a href="" title="Download" id="download-spectrogrampng" download="">
                        <div class="btn download-btn">
                            <i class="fas fa-arrow-down"></i>Download spectrogram PNG
                        </div>
                    </a>

                    <a href="" title="Download" id="download-spectrogrampng_0-03" download="">
                        <div class="btn download-btn">
                            <i class="fas fa-arrow-down"></i>Download spectrogram 0.03 PNG
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
    <!-- end .box -->
</div>
<!-- end .container -->
</body>

</html>