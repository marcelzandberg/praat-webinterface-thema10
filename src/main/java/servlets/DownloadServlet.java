package servlets;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@javax.servlet.annotation.WebServlet(name = "DownloadServlet",  urlPatterns = "/download.get")

/**
 * This program provides downloadable files for the front-end
 *
 * @author Bas Kasemir & Marcel Zandberg (&copy; 2018)
 * @version 1.0.0
 * @since 04-12-2018
 */
public class DownloadServlet extends HttpServlet {

    private final int ARBITARY_SIZE = 1048;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        // Get the parameters that are sent to this servlet and make the outputfolder variable
        String randomhash = request.getParameter("randomhash");
        String fileName = request.getParameter("fileName");
        String outputFolder = null;

        // Check if a filename has an accepted file extension and set the right content type and path.
        // If it has not an accepted file extension, return a text value.
        if (fileName.endsWith(".wav")) {
            response.setContentType("audio/wav");
            response.setHeader("Content-disposition", "attachment; filename="+fileName);
            outputFolder = getServletContext().getInitParameter("PraatUploads") + randomhash + "/";
        } else if (fileName.endsWith(".csv")) {
            response.setContentType("text/csv");
            response.setHeader("Content-disposition", "attachment; filename="+fileName);
            outputFolder = getServletContext().getInitParameter("outputPath") + "/" + randomhash + "/";
        } else if (fileName.endsWith(".png")) {
            response.setContentType("image/png");
            response.setHeader("Content-disposition", "attachment; filename="+randomhash+"_"+fileName);
            outputFolder = getServletContext().getInitParameter("outputPath") + "/" + randomhash + "/";
        } else if (fileName.endsWith(".pdf")) {
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "attachment; filename="+randomhash+"_"+fileName);
            outputFolder = getServletContext().getInitParameter("outputPath") + "/" + randomhash + "/";
        } else {
            ServletOutputStream sout = response.getOutputStream();
            sout.print("Invalid filerequest");
        }

        // Concat the folder and the filename
        String outFile = outputFolder + fileName;

        // I, Bas,  think this code below is obsolete, but I don't wanna break Marcel's work even more.
        HttpSession session = request.getSession(false);
        String sessionid = session.getId();

        // Try to read the file as bytes. Print the stack trace if the file is not found.
        try (InputStream in = new FileInputStream(outFile);
             OutputStream out = response.getOutputStream()) {

            byte[] buffer = new byte[ARBITARY_SIZE];

            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
