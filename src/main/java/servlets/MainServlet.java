package servlets;

import java.io.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


/**
 * This program serves the input from the front-end page(index.jsp)
 * It processes the data, it stores it in a csv file and generates
 * high quality pictures
 *
 * @author Bas Kasemir & Marcel Zandberg (&copy; 2018)
 * @version 1.0.0
 * @since 04-12-2018
 */
//standaard annotatie
@MultipartConfig
@javax.servlet.annotation.WebServlet(name = "MainServlet",  urlPatterns = "/input.do")

public class MainServlet extends javax.servlet.http.HttpServlet {
    private static final String UPLOAD_DIR = "uploads/default_files";
    private static final String UPLOAD_changed_DIR = "uploads/changed_files";

    /**
     *This method is used to get, store and serve commandline options
     *variables for the cmd options
     *@param -w input soundfile
     *@param -o output location
     *@param -advance options
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // web.xml entries with Directory paths
        String praatscriptPath = getServletContext().getInitParameter("PraatScriptPath");
        String outputPath = getServletContext().getInitParameter("outputPath");
        String applicationPath = getServletContext().getInitParameter("applicationPath");
        String PraatUploads = getServletContext().getInitParameter("PraatUploads");


        // gets the input values from the frontend and stores them in a variable
        String randomhash = request.getParameter("randomhash");
        String pat_nr = request.getParameter("pat_nr");
        String date = request.getParameter("date");
        String day = date.substring(0, 2);
        String month = date.substring(2, 4);
        String year = date.substring(4, 8);
        String split_channels = request.getParameter("split_channels");
        String cut_audio = request.getParameter("cut_audio");
        String normalize = request.getParameter("normalize");
        String type_script = request.getParameter("type_script");

        // gets absolute path of the web application
        Path workingDirectory= Paths.get(".").toAbsolutePath();

        // constructs path of the directory to save uploaded file
        String uploadFilePath = applicationPath + UPLOAD_DIR + File.separator + randomhash;
        String upload_renamedFilePath = applicationPath + UPLOAD_changed_DIR + File.separator + randomhash;

        // creates the save directory if it does not exists
        File fileSaveDir = new File(uploadFilePath+File.separator);
        File fileSaveDirhanged = new File(upload_renamedFilePath);
        File fileSaveTemp = new File(PraatUploads + randomhash + "/temp");
        fileSaveTemp.mkdirs();
        if (!fileSaveDir.exists() || fileSaveDirhanged.exists() ){
            fileSaveDir.mkdirs();
            fileSaveDirhanged.mkdirs();
        }




        // Default file name of input file is TASCAM_nummerS12/S34_6decibel.wav

        /*this should be written to the server like PG002_2018.12.25_1234.wav
                                                    PATID_YYYY.MM.DD_Number.wav*/
        String fileName = null;

        // gets extention, construct new filename
        Part file1 = request.getPart("file1");
        fileName = getFileName(file1);
        String numberofrec = fileName.split("_")[1].substring(0,6);
        String fileExtension = fileName.replaceAll(".*\\.", "");
        String newfileName = pat_nr+"_"+year+"."+month+"."+day+"_"+numberofrec;

        // writes file to upload folder
        file1.write(upload_renamedFilePath+File.separator+newfileName+"."+fileExtension);
        String filepath1 = upload_renamedFilePath+File.separator+newfileName+"."+fileExtension;
        System.out.println(filepath1);
        // gets extention, construct new filename of the second file
        Part file2 = request.getPart("file2");
        String numberofrec2 = fileName.split("_")[1].substring(0,6);
        String fileExtension2 = fileName.replaceAll(".*\\.", "");
        String newfileName2 = pat_nr+"_"+year+"."+month+"."+day+"_"+numberofrec2+"_d06";

        // writes file to upload folder
        file2.write(upload_renamedFilePath+File.separator+newfileName2+"."+fileExtension2);


        //checks if the variable is there
        if (pat_nr.isEmpty() || date.isEmpty() || split_channels.isEmpty() || cut_audio.isEmpty() || normalize.isEmpty() || type_script.isEmpty()) {
            System.out.println("something is empty");
            //important for using in error messages
            response.setContentType("text/plain;charset=UTF-8");

            ServletOutputStream sout = response.getOutputStream();
            String content = "Error 503! something went wrong with the input variables. Please fill all the forms in and try again";

            sout.print(content);
        }

        //command line runner
        try {
            Runtime rt = Runtime.getRuntime();
            Process pr = null;
            // makes session folder to store the output
            new File( outputPath + randomhash + "/").mkdirs();
            // checks if option from frontend is checked and if true then use the option
            boolean advanced = false;
            //commandline code to run praat_nogui with change duration
            if(cut_audio.equals("1")){
                Process option  = rt.exec(praatscriptPath + "praat_nogui --run " +praatscriptPath+ "changeduration.praat " + PraatUploads + randomhash);
                advanced = true;
                //commandline code to run praat_nogui with splitchanel praat script
            }else if(split_channels.equals("1")){
                Process option = rt.exec(praatscriptPath + "praat_nogui --run " +praatscriptPath+ "split_channels.praat " + PraatUploads + randomhash);
                advanced = true;
                //commandline code to run praat_nogui with normalze praat script
            }else if(normalize.equals("1")){
            Process option = rt.exec(praatscriptPath + "praat_nogui --run " +praatscriptPath+ "normalize.praat " + PraatUploads + randomhash);
            advanced = true;
        }

            if(advanced){
                //reads file
                File f = new File(PraatUploads+randomhash+"/temp/advanced_"+newfileName2+"."+fileExtension2);
                boolean fe = false;
                while(!fe){
                    //checks if exist so the cmd code can be run
                    if(f.exists() && !f.isDirectory()){
                        //runs over the advaced options map("/temp")
                        pr = rt.exec(praatscriptPath + "praat_nogui --run "+praatscriptPath+"praat_script -25 2 0.3 0 "+PraatUploads+randomhash+"/temp");
                        Process pr1 = rt.exec(praatscriptPath + "praat_test.py -w " + PraatUploads+randomhash+"/temp/advanced_"+newfileName+"."+fileExtension+ " -o " + outputPath + randomhash + "/");
                        fe = true;
                    }
                }
            }else{
                //runs over the normal audiofiles
                pr = rt.exec(praatscriptPath + "praat_nogui --run "+praatscriptPath+"praat_script -25 2 0.3 0 "+PraatUploads+randomhash);
                Process pr1 = rt.exec(praatscriptPath + "praat_test.py -w " + PraatUploads+randomhash+"/"+newfileName+"."+fileExtension+ " -o " + outputPath + randomhash + "/");

            }

            // generates images based on the praat software
            //Process pr1 = rt.exec(praatscriptPath + "praat_test.py -w " + ChangedFilePath+randomhash+"/temp" + " -o " + outputPath + randomhash + "/");

            BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String newfileName3 = pat_nr+"_"+year+"."+month+"."+day;
            String line = null;
            PrintWriter pw = new PrintWriter(new File(outputPath + randomhash + "/" + newfileName3 +".csv"));
            StringBuilder sb = new StringBuilder();

            // appends results to a csv file
            while ((line = input.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }

            pw.write(sb.toString());
            pw.close();

            // stores all the values an results in a json file
            PrintWriter pwjson = new PrintWriter(new File(outputPath + randomhash + "/data.json"));
            StringBuilder sbjson = new StringBuilder();

            sbjson.append("[ {\n");
            sbjson.append("\t\"randomhash\": \""+randomhash+"\",\n");
            sbjson.append("\t\"pat_nr\": \""+pat_nr+"\",\n");
            sbjson.append("\t\"date\": \""+day+"-"+month+"-"+year+"\",\n");
            sbjson.append("\t\"split_channels\": \""+split_channels+"\",\n");
            sbjson.append("\t\"cut_audio\": \""+cut_audio+"\",\n");
            sbjson.append("\t\"normalize\": \""+normalize+"\",\n");
            sbjson.append("\t\"type_script\": \""+type_script+"\",\n");
            sbjson.append("\t\"wav_one\": \""+newfileName+"\",\n");
            sbjson.append("\t\"wav_two\": \""+newfileName2+"\"\n");
            sbjson.append("} ]\n");
            pwjson.write(sbjson.toString());
            pwjson.close();
            int exitVal = pr.waitFor();
            System.out.println("Exited with error code " + exitVal);
            response.sendRedirect("index.jsp#output");
        //catches exception from coomandline runner en throws
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
            RequestDispatcher view;
            view = request.getRequestDispatcher("error.jsp");
            view.forward(request, response);
        }

    }

    /**
     *This method is used to send the user an error message that something is really wrong
     * Go's to error.jsp
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        RequestDispatcher view;
        view = request.getRequestDispatcher("error.jsp");
        view.forward(request, response);
    }

    /**
     * Utility method to get file name from HTTP header content-disposition
     */
    private String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        System.out.println("content-disposition header= " + contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                //System.out.println(token.substring(token.indexOf("=") + 2, token.length() - 1));
                return token.substring(token.indexOf("=") + 2, token.length() - 1);
            }
        }
        return "";
    }
}