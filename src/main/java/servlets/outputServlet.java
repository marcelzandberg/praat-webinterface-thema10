package servlets;


import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Base64;


/**
 * Serves the output for the fron-end output-tab
 *
 * @author Bas Kasemir & Marcel Zandberg (&copy; 2018)
 * @version 1.0.0
 * @since 04-12-2018
 */
@MultipartConfig
@javax.servlet.annotation.WebServlet(name = "outputServlet",  urlPatterns = "/output.get")
public class outputServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //web.xml entries with Directory paths
        String outputPath = getServletContext().getInitParameter("outputPath");
        String apppath = getServletContext().getInitParameter("applicationPath");

        // gets the values from the frontend and stores them in a variable
        String jobID = request.getParameter("jobID");
        String filename = request.getParameter("file");
        String content = null;
        File f = null;
        FileInputStream fis = null;

        response.setContentType("text/plain;charset=UTF-8");

        //encode image to Base64 String

        try {
            f = new File(outputPath + jobID + "/" + filename);     //change path of image according to you
            fis = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            if (filename.endsWith(".png")) {
                f = new File(apppath + "/src/main/webapp/assets/images/outputplaceholder.jpg");
                fis = new FileInputStream(f);
            } else {
                response.setStatus(200);
                ServletOutputStream sout = response.getOutputStream();
                sout.print("Error: File not found");
                return;
            }
        }


            byte byteArray[] = new byte[(int) f.length()];

            fis.read(byteArray);

            //has encoded string of the proccesed image
            byte[] imageBytes = Base64.getEncoder().encode(byteArray);
            String outputString = new String(imageBytes);

            // beetje nasty om eerst te encoden en daarna decoden maar oke
            if (filename.endsWith(".json")) {
                byte [] json = Base64.getDecoder().decode(imageBytes);
                outputString = new String(json);
            }

            fis.close();


            ServletOutputStream sout = response.getOutputStream();
            sout.print(outputString);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");

        //serves message to admin that it works
        ServletOutputStream sout = response.getOutputStream();
        String content = "works get";

        sout.print(content);
    }
}
