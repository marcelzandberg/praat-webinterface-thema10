# Praat Webinterface (Thema 10)

![poster](./Docs/Poster.png "Poster")

This Project is all about building a research web application based on the PRAAT software ([http://www.fon.hum.uva.nl/praat/](http://www.fon.hum.uva.nl/praat/)) In order of the UMC Utrecht and the UMC Groningen.

The web app is built on a Java backend, linked to a dynamic web user-interface. The web app can be used in the browser on your computer and even on your mobile phone / tablet. It's also possible to install it as a prograssive web app on your smartphone / tablet.

All code is kept up-to-date in this Git repository.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Things you need to install the software and how to install them:

* Tomcat server | [Tomcat 9 download page](https://tomcat.apache.org/download-90.cgi)

* Java JRE | [JRE Download page](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

* JavaJDK | [JDK Download page](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

* Python 3 | [Python3 download page](https://www.python.org/downloads/)

* An IDE such as JetBrain's [IntelliJ IDEA](https://www.jetbrains.com/idea/)

* A decent webbrowser such as [Mozilla Firefox](https://www.mozilla.org/firefox/download/)


Install Python packages:

* Open a terminal window *(cmd.exe on windows)*
```
python3   -m pip install SomePackage
```

* parselmouth
```
pip install parselmouth
```
* seaborn
```
pip install seaborn
```
* numpy
```
pip install numpy
```
* matplot
```
pip install matplot
```

### Installing

Now everything is installed, we can set up the web application in a development environment.

* Step 1 set up the environment

  * Clone this repo

  * Start your IDE (we will use IntelliJ IDEA as example here)

  * Click on 'Open project'

  * Check 'Use auto-import' ,'Use default gradle wrapper' and select the Java software, which you have installed earlier, as JDK.

* Step 2 Connect Tomcat server to project

  _Linux, Windows:_
  
  * Go to file... (top left).

  * Click on 'Settings'.

  * Scroll to 'Build, Execution, Deployment' in the sidebar and unfold this section.

  * Go to 'Application Servers' and click the '+' icon to add Tomcat server by selecting the folder where you have extracted the Tomcat server.
  
  _macOS:_
  
  * Go to the IntelliJ menu in the menu bar (next to the ).
  
  * Click on 'Preferences'.
  
  * Scroll to 'Build, Execution, Deployment' in the sidebar and unfold this section.
  
  * Go to 'Application Servers' and click the '+' icon to add Tomcat server by selecting the folder where you have extracted the Tomcat server.

## Deployment

* Step 1 - Set up the server

  * Go to edit configurations (top right in your IntelliJ IDEA window)

  * Select application server Tomcat

  * Set URL: ```http://localhost:8080/index.jsp```

  * Set JRE as  ```Default```

  * **_If in the bottom left of the window a "fix" button appears, press it and check the ```.war(exploded)``` artifact_**

* Step 2 - Change webxml filepaths

  * Open the file web.xml. You can find it here in your cloned repo: ```thema10praat/src/main/webapp/WEB-INF/web.xml```

  * Change the paths to wherever you have extracted the web application folder.
  
  * **NOTE: These paths must be absolute, full paths**
  
  * Example: 
  
    * change: 
    
	  ```<param-name>PraatScriptPath</param-name>```
	  
	  ```<param-value>/homes/mazandberg/bitbucket/themaopdracht-thema-10/thema10praat/Data/</param-value>```
    
    * to: 
    
      ```<param-name>PraatScriptPath</param-name>```
    
	  ```<param-value>/here/the/path/to/thema10praat/Data/</param-value>```
  
 * Step 3 - Start the application

  * Press the little green triangle in the upper right of your screen. This will start the webserver and thereby the application.

  * Your default webbrowser will open a tab with the application. Have fun!

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Font Awesome 5](https://fontawesome.com) - Easy adding icons to the web app
* [JQuery](https://code.jquery.com/) - JavaScript library
* [Gradle dependencies]() - commons-fileupload and commons-io

## Contributing

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

**Frontend**

In this project there are some nice methods in the Javascript and CSS added to make expanding the web app easier. We will list some of them below.

---

### To add a new tab (like the input and output):

  - Add a new tab label in the ```box-head``` class with the class ```tab```.

    ```<div class="tab">[name]</div>```

  - Add a new div beneath the ```box-head``` div. This new div should have the class ```boxcontent``` and the id ```input-[name]``` (where ```[name]``` is the name showed in the tab label).

    ```<div class="boxcontent" id="input-[name]">```

    **Note that the id, and thereby the name of the tab, should be unique.**

--- 

### To add a new popup (like the about and help popup)

- Add an element of your choice with the class ```openpopup-[name]```. This wil be the click trigger for your popup.

  For example: ```<h3 class="openpopup-about">About</h3>```

- Add a div with the classes ```popup``` and ```hideonload``` and the id ```[name]-popup``` (where ```[name]``` is the name specified in the class in the previous point).

  **Note that the id, and thereby the name of the popup, should be unique.**

---

### To add a new switch
- Duplicate the **entire** div ```<div class="checkbox-container">```.

- Fill your new ```[identifier]``` in the following items within the new ```checkbox-container``` div:

  - ```<div class="no_selection checkbox" id="[identifier]">```

  - ```<input type="checkbox" name="[identifier]" value="0" id="input-[identifier]" class="hideonload" checked="">```

- You can use the identifier now in your backend as parameter.

---

### To add visual form validation to a inputfield
- Add the following attribute to your ```<input>```: ```data-req="true"```.

- Specify a minimum length by adding the following attribute as well: ```data-req_min="[number]"```, where ```[number]``` is the minimum length.

## Versioning

We will use [semver](http://semver.org/) for versioning. But since this is the initial release of this software, there aren't any versions available yet.


## Authors

* **Bas Kasemir** - *Initial work* - [Portfolio](https://bioinf.nl/~bhnkasemir) | [Bitbucket](https://bitbucket.org/BasMonkey)

* **Marcel Zandberg** - *Initial work* - [Portfolio](https://bioinf.nl/~mazandberg) | [Bitbucket](https://bitbucket.org/marcelzandberg)

## Licenses

* This project is licensed under the MIT License. See the [LICENCE.md](LICENSE.md) file for further details.

* Parselmouth is released under the GNU General Public License, version 3 or later.

* A [manuscript introducing Parselmouth](https://ai.vub.ac.be/~yajadoul/jadoul_introducing-parselmouth_a-python-interface-to-praat.pdf) has been published in the Journal of Phonetics.

  > Jadoul, Y., Thompson, B., & de Boer, B. (2018). Introducing Parselmouth: A Python interface to Praat. *Journal of Phonetics*, *71*, 1-15. https://doi.org/10.1016/j.wocn.2018.07.001

* Praat Webinterface only exposes Praat's existing functionality and implementation of algorithms. If you use our Praat Webinterface in your research and plan to cite it in a scientific publication, please do not forget to [*cite Praat*](http://www.fon.hum.uva.nl/praat/manual/FAQ__How_to_cite_Praat.html).

  > Boersma, Paul & Weenink, David (2019). Praat: doing phonetics by computer [Computer program]. Version 6.0.46, retrieved 3 January 2019 from http://www.praat.org/

## Acknowledgments

* Thanks to Alban Voppel of the UMC Utrecht for the nice explanation of his wishes.
* Inspiration from StackOverflow and the WebBased course at the Hanze University of Applied Sciences.
* Trello helpt us bigtime with the structure of the project and setting up "what to do".


